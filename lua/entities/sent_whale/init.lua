--Spawned fish. Random model, heal when eaten. No value for DarkRP, in terms of trading.

AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")


include("shared.lua")

function ENT:Initialize()
	self:SetModel("models/apocmodels/SonicGenerations/orca.mdl")
	self:PhysicsInit(SOLID_OBB)
	self:PhysWake()
	self:SetUseType(SIMPLE_USE)
	self:SetCollisionBounds(Vector(-350,-25,-25),Vector(50,25,25))

	
	
	
	self.alive = true
	self.state = "moving"
	
	self.lastDists = {0,0,0,0}
	self.lastDistCounter = 0
	
	self.oxygen = 100 --Percentage
	self.stationaryCounter = 0
	self.lastPos = self:GetPos()
	
	self:GetPhysicsObject():EnableGravity(false)
	self:GetPhysicsObject():SetMass(5000)
	
	self:CallOnRemove("clearWhaleHooks",function() --Used to remove the rotation hooks.
		hook.Remove("Think","upright"..self:EntIndex())
	end)
end

function ENT:SpawnFunction(ply,tr)
	if not tr.Hit then return end
	local ent = ents.Create("sent_whale")
	local spawnpos = nil
	if not tr.Hit then return end

	spawnpos = tr.HitPos
	ent:SetPos(spawnpos+Vector(0,0,50))
	ent:Spawn()
	ent:Activate()
	
	return ent
end

function ENT:OnTakeDamage()
	self.state = "attacked"
	self.attacked = os.clock()
end

function ENT:getDirData()
	local ftrace = util.QuickTrace(self:GetPos(),self:GetForward()*3000,{self})
	local dtrace = util.QuickTrace(self:GetPos(),Vector(0,0,-1)*3000,{self})
	local fdist = (self:GetPos() - ftrace.HitPos):Length()
	local ddist = (self:GetPos() - dtrace.HitPos):Length()
	
	return ftrace,fdist,dtrace,ddist
end

local function handleDistTraces(self,dirs,dist)
	local hFraction_value = 0
	local hFraction_key = nil
	for k,vector in pairs(dirs) do
		local trace = util.QuickTrace(self:GetPos(),vector*dist,{self})
		if not trace.Hit then return {vector,hit=false} end
		if trace.Fraction > hFraction_value then
			hFraction_value = trace.Fraction
			hFraction_key = k
		end
	end
	return {dirs[hFraction_key],hit=true}
end

function ENT:getClearDir(dist)
	local dirs = {
		Vector(1,0,0),
		Vector(0,1,0),
		Vector(-1,0,0),
		Vector(0,-1,0),
		Vector(1,1,0),
		Vector(1,-1,0),
		Vector(-1,1,0),
		Vector(-1,-1,0)
	}
	return handleDistTraces(self,dirs,dist)
	
end

function ENT:updateddist(ddist)
	--Updating the distances. Push the table back, then add the new one at the front.
	--There was probably a premade function for this. But meh
	for i=(#self.lastDists-1),1,-1 do
		self.lastDists[i+1] = self.lastDists[i]
	end
	self.lastDists[1] = ddist
end

function ENT:makeUpright()
	local physObj = self:GetPhysicsObject()
	local function handleUpright()
		local angles = self:GetAngles()
		physObj:AddAngleVelocity(Vector(-physObj:GetAngleVelocity().x,0,0))
		if(angles.r > 5) then
			physObj:AddAngleVelocity(Vector(-50,0,0))
		elseif (angles.r < -5) then
			physObj:AddAngleVelocity(Vector(50,0,0))
		else
			hook.Remove("Think","upright"..self:EntIndex())
		end
	end
	
	hook.Add("Think","upright"..self:EntIndex(),handleUpright)
end

function ENT:rotateTo(pitch,yaw) --No roll. Rolling screws with things in a bad way.
	local physObj = self:GetPhysicsObject() --Get the physics object
	function performRotate() --Function so I could make it a seperate hook later.
		local angles = self:GetAngles()
		local vels = {} --Velocities. I'm too lazy to use the full word.
		--Convert to 360 degree format. Leave roll as it is, as we don't need to change it.
		if angles.p < 0 then
			angles.p = 360 + angles.p
		end
		if angles.y < 0 then
			angles.y = 360 + angles.y
		end
	
		--Get the differences between the angles
		local pdifference = pitch - angles.p
		local ydifference = yaw - angles.y
		
		--If they're less than 0, then the angle is clockwise, rather than the normal anticlockwise.
		--We need it to be consistent, so convert it back.
		if pdifference < 0 then
			pdifference = 360 + pdifference
		end
		if ydifference < 0 then
			ydifference = 360 + ydifference
		end
		
		--Choose direction to rotate in. Always pick smallest direction (logic, right?).
		--+- 5 tolerance on the angles, else there's annoying visual effects. If they appear to 'bounce'
		--between two angles either side of the one you want, increase the tolerance.
		if pdifference > 5 and pdifference < 180  then
			vels.p = 50
		elseif pdifference < 355 and  pdifference > 180 then
			vels.p = -50
		else
			vels.p = 0
		end
			
		if ydifference > 5 and ydifference < 180 then
			vels.y = 50
		elseif ydifference < 355 and  ydifference > 180 then
			vels.y = -50
		else
			vels.y = 0
		end
		
		--If we're upside down, then the directions we need to rotate in 'locally' in order to reach the
		--desired 'world' angle will be reversed. Fuck whomever decided to put the angular velocity and 
		--actual angles on different sets of axis.
		if angles.r > 90 or angles.r < -90 then
			vels.p = vels.p * -1
			vels.y = vels.y * -1
		end
		
		--Set the angular velocity to 0. We want a clean slate so we know the exact rotation speeds.
		--Again, fuck whomever decided to not have a 'SetAngleVelocity' function.
		physObj:AddAngleVelocity(-physObj:GetAngleVelocity())
		
		--If we're within the 5 degree tolerance, return! 
		if vels.p == 0 and vels.y == 0 then return true end
		
		physObj:AddAngleVelocity(Vector(0,vels.p,vels.y))
		
		--And thus ends the product of three days of work.
	end
	
	return performRotate()
end
	
local function ddistShortening(self)
	if not (self.lastDists[1] and self.lastDists[2]) then return false end
	for i=2,#self.lastDists do
		if not self.lastDists[i] then return false end
		if self.lastDists[i] <= self.lastDists[i-1]+10 then --If the distances are getting larger.
			return false
		end
	end
	return true
end	

local function angleTo(self,x,y,z) --Convinience function.
	local vec = Vector(x,y,z) - self:GetPos()
	return vec:Angle()
end

function ENT:handleOxygen(ddist)
	if self:GetPos().z >= (self.surface - 50) then 
		if self.oxygen >= 100 then
			local modifier = math.random(300,600)
			if modifier < ddist then
				self.cruiseDepth = self.surface - modifier
			end
		else
			self.oxygen = self.oxygen + 4
		end
	else
		self.oxygen = self.oxygen - 1
		if self.oxygen <= 0 then
			self.oxygen = 0
			self.cruiseDepth = self.surface - 20
		end
	end
end

function ENT:handleWaterSurface()
	if not self.surface then
		if self:WaterLevel() > 0 then
			self.surface = self:GetPos().z
		else
			self.surface = -2000 --A stupidly low value, just so we can iterate up to a proper one. This is kind of a fallback default. I'd rather have a proper one though.
		end
	end
			
	if self:WaterLevel() > 0 then
		if self:GetPos().z > self.surface then --If we constantly check, hopefully we'll iteratively get nearer the true value.
			self.surface = self:GetPos().z
		end
	end
end

function ENT:stationaryCheck()
	--Breaking this up into 3 statements for neatness.
	local incremented = false
	if self:GetPos().x > (self.lastPos.x - 50) and self:GetPos().x < (self.lastPos.x + 50) then
		if self:GetPos().y > (self.lastPos.y - 50) and self:GetPos().y < (self.lastPos.y + 50) then
			if self:GetPos().z > (self.lastPos.z - 50) and self:GetPos().z < (self.lastPos.z + 50) then
				--We might be stuck! Let's warn ourselves.
				self.stationaryCounter = self.stationaryCounter + 1
				incremented = true
			end
		end
	end

	if not incremented then
		self.stationaryCounter = 0
	end
	
	if self.stationaryCounter > 10 then
		self.state = "reversing"
	end
	
	self.lastPos = self:GetPos()
end

function ENT:handleDryness()
	if self:WaterLevel() == 0 then
		if self.dryStart then
			if (os.clock() - self.dryStart) > 1 then
				self.state = "dry"
			end
		else
			self.dryStart = os.clock()
		end
	end
end
				
	
function ENT:Think()
	if not self.alive then return end
	local ftrace,fdist,dtrace,ddist = self:getDirData()
	local physObj = self:GetPhysicsObject()
	
	physObj:SetBuoyancyRatio(0) --This needs to be called every Think, else it gets reset. Who knows why. Bloody gmod.
	
	self:updateddist(ddist)
	--Assign a cruise depth if none exists
	if not self.cruiseDepth then
		if self.surface then self.cruiseDepth = self.surface
		else self.cruiseDepth = -50
		end
	end
	
	--Handle dryness
	self:handleDryness()
	
	--Assigning surface values and managing them.
	self:handleWaterSurface()
	
	
	--Handling the oxygen levels. 
	self:handleOxygen(ddist)
	
	--Check we're not stuck
	self:stationaryCheck()
	local cruiseAngle = angleTo(self,self:GetPos().x + self:GetForward().x*500,self:GetPos().y+ self:GetForward().y*500,self.cruiseDepth)
				
	if self.state == "moving" then 
		if 1 or self:WaterLevel() >= 2 then --If we're mostly submerged
			if ddistShortening(self) and  ddist < 800 then --If we're heading towards land
				local choices = {150,300}
				self.shoreRotation = (self:GetAngles().y+choices[math.random(1,2)])%360
				self.state = "avoidingShore"
			elseif fdist < 800 then --If we're heading towards another object.
				self.state = "avoiding"
				return
			else --If we're at sea, happy as can be.
				self:makeUpright()
				self:rotateTo(cruiseAngle.p,self:GetAngles().y+math.random(-10,10))
				physObj:SetVelocity(self:GetForward()*500)
			end
		end
	elseif self.state == "avoiding" then
		if fdist > 800 then
			self.state = "moving"
		end
		local clear = self:getClearDir(1000)
		local angle = clear[1]:Angle()
		self:rotateTo(0,angle.y)
		physObj:SetVelocity((self:GetForward())*300)
	elseif self.state == "avoidingShore" then
		if self:rotateTo(0,self.shoreRotation) then
			self.state = "moving"
		end
		physObj:SetVelocity((self:GetForward())*300)
	elseif self.state == "reversing" then
		if fdist > 500 then
			self.state = "avoiding"
		end
		self:rotateTo(0,self:GetAngles().y)
		physObj:SetVelocity((self:GetForward())*-300)
	elseif self.state == "dry" then
		physObj:EnableGravity(true)
		if self:WaterLevel() > 0 then
			self.state = "moving"
		end
	end
	
	--Get rising working properly. Maybe direct it towards the surface marker? Change Vector(0,0,-40) to a config constant.
	
end 
	




scripted_ents.Register( ENT, "sent_whale")