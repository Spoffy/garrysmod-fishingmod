--Spawned fish. Random model, heal when eaten. No value for DarkRP, in terms of trading.

AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")


include("shared.lua")

local models = {"fishbass.mdl","fishcatfish.mdl","fishgolden.mdl","fishrainbow.mdl"}
local minJumpDelay = 1
local maxJumpDelay = 4
local minJumpVelocity = 50
local maxJumpVelocity = 200
local minLife = 10
local maxLife = 10
local healVal = 10

function ENT:Initialize()
	self:SetModel("models/FoodNHouseholdItems/"..models[math.random(1,#models)])
	self:PhysicsInit(SOLID_VPHYSICS)
	self:PhysWake()
	self:SetUseType(SIMPLE_USE)
	
	self.alive = true
	self.deathTime = os.clock() + math.random(minLife,maxLife)
	self.FoodEnergy = 5
end

function ENT:SpawnFunction(ply,tr)
	if not tr.Hit then return end
	local ent = ents.Create("sent_fish")
	local spawnpos = nil
	if not tr.Hit then return end

	spawnpos = tr.HitPos
	ent:SetPos(spawnpos+Vector(0,0,50))
	ent:Spawn()
	ent:Activate()
	
	return ent
end

ENT.nextJump = 0
ENT.lastHit = Vector(0,0,-1)

function ENT:PhysicsCollide(data,physObj)
	self.lastHit = data.HitNormal
end

function ENT:Think()
	if os.clock() > self.nextJump and self:WaterLevel() > 0 then
		if self.alive then
			self:GetPhysicsObject():SetBuoyancyRatio(0)
		else 
			self:GetPhysicsObject():SetBuoyancyRatio(0.5)
		end
		self.nextJump = os.clock() + math.random(minJumpDelay,maxJumpDelay)
	elseif self.alive and (os.clock() > (self.nextJump)) and self:GetVelocity():Length() == 0 then
		self:GetPhysicsObject():AddVelocity(self.lastHit*math.random(minJumpVelocity,maxJumpVelocity)*-1) --Negative one is due to the HitNormal being directed TOWARDS the entity hit.
		self:GetPhysicsObject():AddAngleVelocity(Vector(math.random(1,300),math.random(1,300),math.random(1,300)))
		if os.clock() > self.deathTime then
			self.alive = false
			return
		end
		self.nextJump = os.clock() + math.random(minJumpDelay,maxJumpDelay)
	end 
	
	 
end

function ENT:Use(activator,caller,useType,value)
	if not activator then return end
	if not activator:IsPlayer() then return end
	if GAMEMODE_NAME == "darkrp" then
		activator.DarkRPVars = activator.DarkRPVars or {}
		activator:SetSelfDarkRPVar("Energy", math.Clamp((activator.DarkRPVars.Energy or 100) + (self:GetTable().FoodEnergy or 1), 0, 100))
		umsg.Start("AteFoodIcon", activator)
		umsg.End()
		self:Remove()
		activator:EmitSound("vo/sandwicheat09.wav", 100, 100)
	else
		activator:SetHealth(min(activator:Health()+healVal,100))
	end
	self:Remove()
end

function ENT:HandleFishTransfer(crate)
	local fishTable = {{model = self:GetModel(),val = self.val}} --Double table, as the info is the first entry.
	local transfers = crate:TransferFish(fishTable)
	if transfers > 0 then
		self:Remove()
	end
end
	




scripted_ents.Register( ENT, "sent_fish")