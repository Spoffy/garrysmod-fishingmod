AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")


include("shared.lua")

local maxFishValue = 2 --Max 4. Most valuable fish a net can catch.

function ENT:Initialize()
	self:SetModel("models/props_foliage/hedge_128.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:PhysWake()
	self:SetUseType(SIMPLE_USE)
	
	self.inWater = false --This allows us to check for changes, unlike just using WaterLevel()
	self.nextThink = 0
end

function ENT:SpawnFunction(ply,tr)
	if not tr.Hit then return end
	local ent = ents.Create("sent_net")
	local spawnpos = nil
	if not tr.Hit then return end

	spawnpos = tr.HitPos
	ent:SetPos(spawnpos+Vector(0,0,50))
	ent:Spawn()
	ent:Activate()
	
	return ent
end

local function SetFishValue(fish,val)
	local model = "models/FoodNHouseholdItems/fishbass.mdl" --Default
	if val == 1 then
		model = "models/FoodNHouseholdItems/fishbass.mdl"
	elseif val == 2 then
		model = "models/FoodNHouseholdItems/fishgolden.mdl"
	elseif val == 3 then
		model = "models/FoodNHouseholdItems/fishcatfish.mdl"
	elseif val == 4 then
		model = "models/FoodNHouseholdItems/fishrainbow.mdl"
	end
	
	if fish then
		fish:SetModel(model)
		fish.valueLevel = val
	end
	
	return model,val
	
end

function ENT:onWaterLevelChange()
	
end

function ENT:HandleSubmersion()
	local prob = self:GetVelocity():Length()/20 --Calculate probability of a fish capture. Maxes at 100% at root(divisor*100) seconds.
	local chance = math.random(1,100)
	--print("Prob: "..prob.." Chance:" .. chance)
	if chance <= prob then
		self:SetNWInt("fishCaught",self:GetNWInt("fishCaught",0) + 1)
	end
end

function ENT:Think()
	if os.clock() < self.nextThink then return end
	self.nextThink = os.clock() + 0.5
	if self:WaterLevel() > 0 and not self.inWater then
		self.inWater = true
		self:onWaterLevelChange()
	elseif self:WaterLevel() == 0 and self.inWater then
		self.inWater = false 
		self:onWaterLevelChange()
	end
	
	if self.inWater then
		self:HandleSubmersion()
	end

end

function ENT:Use()
	local fishCaught = self:GetNWInt("fishCaught",0)
	if fishCaught == 0 then return end
	for i = 1,fishCaught do
		local ent = ents.Create("sent_fish")
		ent:SetPos(self:GetPos() + Vector(math.random(-150,150),math.random(-150,150),10))
		ent:Spawn()
		ent:Activate()
		SetFishValue(ent,math.random(1,maxFishValue))
	end
	self:SetNWInt("fishCaught",0)
		
end

function ENT:HandleFishTransfer(crate)
	local fishTable = {}
	for i=1,self:GetNWInt("fishCaught",0) do
		local model,val = SetFishValue(nil,math.random(1,maxFishValue))
		table.insert(fishTable,{model=model,val=val})
	end
	local transfers = crate:TransferFish(fishTable)
	self:SetNWInt("fishCaught",self:GetNWInt("fishCaught",0) - transfers)
end

scripted_ents.Register( ENT, "sent_net")