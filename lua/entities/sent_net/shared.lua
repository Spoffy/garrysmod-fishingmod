ENT.Base = "base_anim"
ENT.Type = "anim"

ENT.PrintName = "Fishing Net"
ENT.Author = "Spoffy (Gamer_cad)"
ENT.Information = "You fish with it."
ENT.Category = "Spoffy's Custom SENTs"

ENT.Spawnable = true
ENT.AdminSpawnable = false