include("shared.lua")

local function drawNet()
	local trace = LocalPlayer():GetEyeTrace()
	if trace.Hit and trace.Entity:GetClass() == "sent_net" then
		local net = trace.Entity
		local netPos = net:GetPos()
		if (netPos-LocalPlayer():GetPos()):Length() > 1000 then return end
		local screenPos = netPos:ToScreen()
		if not screenPos.visible then return end
		draw.DrawText("Net - Fish: ".. net:GetNWInt("fishCaught",0),"Default",screenPos.x,screenPos.y,Color(200,200,200),TEXT_ALIGN_CENTER)
	end
end


hook.Add("HUDPaint","fishingDrawNet",drawNet)