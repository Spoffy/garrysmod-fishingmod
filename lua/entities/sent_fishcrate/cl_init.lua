include("shared.lua")

local function drawCrate()
	local trace = LocalPlayer():GetEyeTrace()
	if trace.Hit and trace.Entity:GetClass() == "sent_fishcrate" then
		local crate = trace.Entity
		local cratePos = crate:GetPos()
		if (cratePos-LocalPlayer():GetPos()):Length() > 1000 then return end
		local screenPos = cratePos:ToScreen()
		if not screenPos.visible then return end
		draw.DrawText("Fish: ".. crate:GetNWInt("fish",0),"Default",screenPos.x,screenPos.y,Color(200,200,200),TEXT_ALIGN_CENTER)
	end
end


hook.Add("HUDPaint","fishingDrawCrate",drawCrate)