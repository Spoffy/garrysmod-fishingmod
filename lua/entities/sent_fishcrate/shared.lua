ENT.Base = "base_anim"
ENT.Type = "anim"

ENT.PrintName = "Fish Crate"
ENT.Author = "Spoffy (Gamer_cad)"
ENT.Information = "Store fish in this."
ENT.Category = "Spoffy's Custom SENTs"

ENT.Spawnable = true
ENT.AdminSpawnable = false