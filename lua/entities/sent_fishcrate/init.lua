AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")


include("shared.lua")


function ENT:Initialize()
	self:SetModel("models/props_junk/wood_crate001a.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:PhysWake()
	self:SetUseType(SIMPLE_USE)
	
	self.nextThink = 0
	self.fish = {}
end

function ENT:SpawnFunction(ply,tr)
	if not tr.Hit then return end
	local ent = ents.Create("sent_fishcrate")
	local spawnpos = nil
	if not tr.Hit then return end

	spawnpos = tr.HitPos
	ent:SetPos(spawnpos+Vector(0,0,50))
	ent:Spawn()
	ent:Activate()
	
	return ent
end

function ENT:Think()
	if os.clock() < self.nextThink then return end
end

function ENT:TransferFish(fish)
	local transfers = 0
	for key,value in pairs(fish) do
		table.insert(self.fish,value)
		transfers = transfers + 1 --Some basic checks to make sure everything got transferred
	end
	self:SetNWInt("fish",self:GetNWInt("fish",0)+transfers)
	return transfers
end

function ENT:StartTouch(entity)
	if entity.HandleFishTransfer then
		entity:HandleFishTransfer(self)
	end
end

function ENT:Use()
	if #self.fish == 0 then return end
	for index,fish in pairs(self.fish) do
		local ent = ents.Create("sent_fish")
		ent:SetPos(self:GetPos() + Vector(math.random(-150,150),math.random(-150,150),10))
		ent:Spawn()
		ent:Activate()
	end
	self.fish = {}
	self:SetNWInt("fish",0)
end

scripted_ents.Register( ENT, "sent_fishcrate")