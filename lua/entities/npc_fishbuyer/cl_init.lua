include("shared.lua")

local function sellFishMenu(quantity,value,seller)
	local scrH = ScrH()
	local scrW = ScrW()
	
	local frame = vgui.Create( "DFrame")
	frame:SetSize(scrW/5,scrH/5)
	frame:SetPos((scrW-frame:GetWide())/2,(scrH-frame:GetTall())/2)
	frame:SetTitle("Sell Fish")
	frame:SetSizable(false)
	
	local sellButton = vgui.Create( "DButton", frame )
	sellButton:SetSize(frame:GetWide()*0.8,frame:GetTall()*0.2)
	sellButton:SetPos((frame:GetWide()-sellButton:GetWide())/2,frame:GetTall()*0.7)
	sellButton:SetText("Sell Fish")
	
	sellButton.DoClick = function()
		net.Start("fishBuyerReply")
		net.WriteInt(seller,10)
		net.SendToServer()
		frame:Remove()
	end
	
	local fishNumber = vgui.Create( "DLabel", frame )
	fishNumber:SetFont("BudgetLabel")
	local fishNumberText = "Fish: " .. quantity
	fishNumber:SetText(fishNumberText)
	
	fishNumber:SetSize(string.len(fishNumberText)*7,frame:GetTall()*0.2)
	fishNumber:SetPos((frame:GetWide()-fishNumber:GetWide())/2,frame:GetTall()*0.2)
	
	local fishValue = vgui.Create( "DLabel", frame )
	fishValue:SetFont("BudgetLabel")
	local fishValueText = "Value: $" .. value
	fishValue:SetText(fishValueText)
	
	fishValue:SetSize(string.len(fishValueText)*7,frame:GetTall()*0.2)
	fishValue:SetPos((frame:GetWide()-fishValue:GetWide())/2,frame:GetTall()*0.35)
	
	
	frame:MakePopup()
end

local function handleFishBuyer()
	local quantity = net.ReadInt(10)
	local value = net.ReadInt(20)
	local seller = net.ReadInt(10)
	sellFishMenu(quantity,value,seller)
end

net.Receive("fishBuyerMenu",handleFishBuyer)

--Popup Message when the mouse is over them.

local function drawHudImage()
	local trace = LocalPlayer():GetEyeTrace()
	if IsValid(trace.Entity) and trace.Entity:GetClass() == "npc_fishbuyer" then
		draw.SimpleText("Fish Buyer (E to use)","ChatFont",ScrW()/2+5,ScrH()/2+5,Color(200,200,200),TEXT_ALIGN_LEFT,TEXT_ALIGN_TOP)
	end
end

hook.Add("HUDPaint","paintFishBuyerOption",drawHudImage)

