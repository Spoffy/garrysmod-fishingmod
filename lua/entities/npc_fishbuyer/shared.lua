ENT.Base = "base_ai"
ENT.Type = "ai"

ENT.PrintName = "NPC Fishbuyer"
ENT.Author = "Spoffy (Gamer_cad)"
ENT.Information = "Fish buying NPC."
ENT.Category = "Spoffy's Custom SENTs"

ENT.Spawnable = true
ENT.AdminSpawnable = false