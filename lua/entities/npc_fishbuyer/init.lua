AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")

include("shared.lua")
include("saveload.lua")

local saleRange = 100

local mf = {"male","female"}

function ENT:Initialize()
	self:SetModel( "models/humans/group0" .. math.random(1,3) .. "/" .. mf[math.random(1,2)] .. "_" .. "0"..math.random(1,7) .. ".mdl" ) -- Sets the model of the NPC.
	self:SetHullType( HULL_HUMAN ) -- Sets the hull type, used for movement calculations amongst other things.
	self:SetHullSizeNormal( )
	self:SetNPCState( NPC_STATE_SCRIPT )
	self:SetSolid(  SOLID_BBOX ) -- This entity uses a solid bounding box for collisions.
	--self:SetCollisionBounds(Vector(-25,-15,0),Vector(25,15,60))
	self:CapabilitiesAdd(bit.bor(CAP_ANIMATEDFACE, CAP_TURN_HEAD )) -- Adds what the NPC is allowed to do ( It cannot move in this case ).
	self:SetUseType( SIMPLE_USE ) -- Makes the ENT.Use hook only get called once at every use.
	self:DropToFloor()
 
	self:SetMaxYawSpeed( 90 ) --Sets the angle by which an NPC can rotate at once.
end

function ENT:SpawnFunction(ply,tr)
	if not tr.Hit then return end
	local ent = ents.Create("npc_fishbuyer")
	local spawnpos = nil
	if not tr.Hit then return end

	spawnpos = tr.HitPos
	ent:SetPos(spawnpos)
	ent:Spawn()
	ent:Activate()
	
	return ent
end

function ENT:OnTakeDamage(damage)
	return false
end

util.AddNetworkString("fishBuyerMenu")
util.AddNetworkString("fishBuyerReply")

--Networking/Interaction Code

function ENT:AcceptInput(iName,activator,caller)
	if iName == "Use" then
		local ents = ents.FindInSphere(self:GetPos(),saleRange)
		local quantity = 0
		local value = 0
		for k,ent in pairs(ents) do
			if ent:GetClass() == "sent_fish" then
				quantity = quantity + 1
				value = value + (ent.valueLevel or 1)*GetGlobalInt("fishValueMultiplier")
			elseif ent:GetClass() == "sent_fishcrate" and ent.fish and #ent.fish then
				if not ent.Owner or ent.Owner == activator then
					for i,fish in pairs(ent.fish) do
						quantity = quantity + 1
						value = value + (fish.val or 1)*GetGlobalInt("fishValueMultiplier")
					end
				end
			elseif ent:GetClass() == "sent_whale" then
				quantity = quantity + 1 
				value = value + GetGlobalInt("whaleBonus")
			end
		end
		net.Start("fishBuyerMenu")
		net.WriteInt(quantity,10)
		net.WriteInt(value,20)
		net.WriteInt(self:EntIndex(),10)
		net.Send(activator)
	end
end

local function saleRequest(length,player)
	local index = net.ReadInt(10)  
	local fishBuyer = ents.GetByIndex(index)
	if (player:GetPos()-fishBuyer:GetPos()):Length() > 100 then return end
	local ents = ents.FindInSphere(fishBuyer:GetPos(),saleRange)
	local money = 0
	for k,ent in pairs(ents) do
		if ent:GetClass() == "sent_fish" then
			money = money + (ent.valueLevel or 1)*GetGlobalInt("fishValueMultiplier")
			ent:Remove()
		elseif ent:GetClass() == "sent_fishcrate" and ent.fish and #ent.fish > 0 then
			if not ent.Owner or ent.Owner == player then
				for i=#ent.fish,1,-1 do
					fish = ent.fish[i]
					money = money + (fish.valueLevel or 1)*GetGlobalInt("fishValueMultiplier")
					table.remove(ent.fish,i)
				end
				ent:SetNWInt("fish",0)
			end
		elseif ent:GetClass() == "sent_whale" then
			money = money + GetGlobalInt("whaleBonus")
			ent:Remove()
		end
	end
	
	player:AddMoney(money)
	player:ChatPrint("You have been paid $" .. money .. " for your fish!")
	
end

net.Receive("fishBuyerReply",saleRequest)


----------------------

scripted_ents.Register( ENT, "npc_fishbuyer")