local function loadFishBuyers()
	if not file.Exists("FishingMod/npc_fishbuyers/".. game.GetMap()..".txt","DATA") then return end
	local rawSave = file.Read("FishingMod/npc_fishbuyers/"..game.GetMap()..".txt","DATA")
	if not rawSave then return end
	local saves = util.JSONToTable(rawSave)
	for k,ent in pairs(ents.FindByClass("npc_fishbuyer")) do
		ent:Remove()
	end
	for k,pos in pairs(saves) do
		local seller = ents.Create("npc_fishbuyer")
		seller:SetPos(pos)
		seller:Spawn()
		seller:Activate()
	end
end

local function saveFishBuyers(ply)
	if not (ply:IsAdmin() or ply:IsSuperAdmin()) then return end
	local ents = ents.FindByClass("npc_fishbuyer")
	local saves = {}
	for k,buyer in pairs(ents) do
		table.insert(saves,buyer:GetPos())
	end
	local JSON = util.TableToJSON(saves)
	if not file.Exists("FishingMod","DATA") then
		file.CreateDir("FishingMod","DATA")
	end
	if not file.Exists("FishingMod/npc_fishbuyers","DATA") then
		file.CreateDir("FishingMod/npc_fishbuyers","DATA")
	end
	file.Write("FishingMod/npc_fishbuyers/"..game.GetMap()..".txt",JSON)
end

concommand.Add("save_fishbuyers",saveFishBuyers)

hook.Add("InitPostEntity","loadFishBuyers",function() --Delay the loading, else they sometimes get deleted for no apparent reason.
	timer.Simple(3,loadFishBuyers)
end
)