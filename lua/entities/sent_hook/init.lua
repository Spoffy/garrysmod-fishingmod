AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")


include("shared.lua")


function ENT:Initialize()
	self:SetModel("models/props_junk/meathook001a.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:PhysWake()
	self:SetUseType(SIMPLE_USE)
	
	self.inWater = false --This allows us to check for changes, unlike just using WaterLevel()
	self.waterEntered = nil -- Time the hook went into the water.
	self.startDepth = nil -- Needs to be set when the hook enters the water. This'll allow us to get relative distance, and thus calculate value.
	self.nextThink = 0
	self.fish = nil --Set this when we catch a fish.
end

function ENT:SpawnFunction(ply,tr)
	if not tr.Hit then return end
	local ent = ents.Create("sent_hook")
	local spawnpos = nil
	if not tr.Hit then return end

	spawnpos = tr.HitPos
	ent:SetPos(spawnpos+Vector(0,0,50))
	ent:Spawn()
	ent:Activate()
	
	return ent
end

local function SetFishValue(fish,val)
	local model = "models/FoodNHouseholdItems/fishbass.mdl" --Default
	if val == 1 then
		model = "models/FoodNHouseholdItems/fishbass.mdl"
	elseif val == 2 then
		model = "models/FoodNHouseholdItems/fishgolden.mdl"
	elseif val == 3 then
		model = "models/FoodNHouseholdItems/fishcatfish.mdl"
	elseif val == 4 then
		model = "models/FoodNHouseholdItems/fishrainbow.mdl"
	end
	
	fish:SetModel(model)
	fish.valueLevel = val
	
end

function ENT:CatchFish()
	local ent = ents.Create("sent_fish")
	ent:SetPos(self:LocalToWorld(Vector(0.535164, -7.365694, -13.875528)))
	ent:Spawn()
	ent:Activate()
	constraint.Weld(self,ent,0,0,false,true,false) --Args: Ent1,Ent2,Bone1,Bone2,BreakForce,nocollide,deleteOnRemove
	self.fish = ent
	if not self.startDepth then return end
	local relativeDepth = self.startDepth - self:GetPos().z
	local valueBand = math.abs(relativeDepth/300) --This is the depth over a constant, which is the width of a band, and represents what type of fish should be available. Bigger is deeper, therefore more valuable.
	local valProb = math.random(1,100)
	if valueBand < 1 then --Band 1
		SetFishValue(self.fish,1)
	elseif valueBand < 2 then ---Band 2
		if valProb < 40 then
			SetFishValue(self.fish,1)
		else
			SetFishValue(self.fish,2)
		end
	elseif valueBand < 3 then --Band 3
		if valProb < 10 then
			SetFishValue(self.fish,1)
		elseif valProb < 40 then
			SetFishValue(self.fish,2)
		else
			SetFishValue(self.fish,3)
		end
	else --Band 4
		if valProb < 10 then
			SetFishValue(self.fish,1)
		elseif valProb < 25 then
			SetFishValue(self.fish,2)
		elseif valProb < 40 then
			SetFishValue(self.fish,3)
		else
			SetFishValue(self.fish,4)
		end
	end
end

function ENT:onWaterLevelChange()
	if self.inWater then
		self.waterEntered = os.clock()
		self.startDepth = self:GetPos().z
	else
		self.waterEntered = nil
		self.startDepth = nil
	end
	
end

function ENT:HandleSubmersion()
	local waterTime = os.clock() - self.waterEntered --The time we've been in the water.
	local prob = (waterTime * waterTime)/144 --Calculate probability of a fish capture. Maxes at 100% at root(divisor*100) seconds.
	local chance = math.random(1,100)
	--print("Watertime:" .. waterTime .. "Prob: "..prob.." Chance:" .. chance)
	if not IsValid(self.fish) and chance <= prob then
		self:CatchFish()
	end
end

function ENT:Think()
	if os.clock() < self.nextThink then return end
	self.nextThink = os.clock() + 0.5
	if self:WaterLevel() > 0 and not self.inWater then
		self.inWater = true
		self:onWaterLevelChange()
	elseif self:WaterLevel() == 0 and self.inWater then
		self.inWater = false
		self:onWaterLevelChange()
	end
	
	if IsValid(self.fish) then --Fixing an issue whereby removing fish while the hook is in the water wouldn't reset the probability.
		self.waterEntered = os.clock()
	end
	
	if self.inWater then
		self:HandleSubmersion()
	end

end

function ENT:Use()
	if self.fish then
		local constraint = constraint.Find(self,self.fish,"Weld",0,0)
		if not constraint then return end
		constraint:Remove()
		self.fish = nil
	end
end

scripted_ents.Register( ENT, "sent_hook")