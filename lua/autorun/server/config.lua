--Use this file to configure aspects of the addon. 

local valueMultiplier = 15 --The factor by which the fish values (1,2,3,4) are multiplied. 
local whaleBonus = 10000 --The reward for capturing a whale.


--Performing some basic pre-game setups/
SetGlobalInt("fishValueMultiplier",valueMultiplier)
SetGlobalInt("whaleBonus",whaleBonus)